import React from 'react';
import { StyleSheet, Text, View,Button,TextInput } from 'react-native';
import { AppRegistry, Image } from 'react-native';

export default class App extends React.Component {
  
  

  constructor(props) {
      super(props);
      this.state = {
        searchString: 'welcome'
      };
    }
  
    _onSearchTextChanged = (event) => {
      console.log('Typing...');
      this.setState({ searchString: event.nativeEvent.text });
      //console.log('Current: '+this.state.searchString+', Next: '+event.nativeEvent.text);
    };
 
 
  _onSearchPressed = () => {
    
         const query = urlForQueryAndPage('place_name', this.state.searchString, 1);
    
         this._executeQuery(query);
    
       }; 
   
    
     testing1 = () => {
    
       console.log("Function reached");
    
     };

     returnapiurl = () => {
      const url = 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/';
      return url;
    }

    /*_executeQuery = (apiurl) => {
      console.log("reached _executeQuery and the query is " + apiurl);
  
      fetch(apiurl)
      .then((response) => response.json())
      .then(console.log(response))
      .catch(error =>
         {
           console.log("something went wrong with fetch");
         });
    }*/

    XMLQuery = (apiurl) => {
      var request = new XMLHttpRequest();
      request.onreadystatechange = (e) => {
        if (request.readyState !== 4) {
          return;
        }
      
        if (request.status === 200) {
          console.log('success', request.responseText);
        } else {
          console.warn('error');
        }
      };
      
      request.open('GET', 'http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/');
      request.send();
    }


    

  render() {

    return (

      <View style={styles.container}>

        <Text style={{fontSize:40}}>Creed</Text>
        <View style={styles.flowRight}>
        <TextInput
        style={styles.searchInput}
        value={this.state.searchString}

           onChange={this._onSearchTextChanged}

             placeholder='type anything'/>
             <Button
                           onPress={() => {
                             console.log(this.state.searchString);
                           }}
                            color='#48BBEC'
                            title='Hit me'
                          /> 
                          </View>
        
        <Image source={require('./Creed.jpg')} style={styles.container2}/>

        <Button
             onPress={() => {
             this.testing1();
             }}
             title='Get Function'
             />

             <Button
            onPress={() => {
              console.log("button 2 pressed");
              const apiurl = this.returnapiurl();
              console.log("URL for API is "+apiurl);
              //this._executeQuery(apiurl);
              this.XMLQuery(apiurl);
            }}
            color='#48BBEC'
            title='Hit me 2'
          />
          
       </View>    

      

    );
   
    

  }

}


const styles = StyleSheet.create({

  container: 
    {
  
       backgroundColor: '#d4f8f7',
      
       alignItems: 'center',
  
       justifyContent: 'center',
  
      padding: 5,
  
      marginTop: 5,
     
  
    },
    container2:
    {
      width:480,
      height:300,
    },
    searchInput: {
      height: 46,
      width:200,
      padding: 10,
      marginRight: 5,
      flexGrow: 1,
      fontSize: 18,
      borderWidth: 5,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#56BBEC',
    },
    flowRight: {
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'stretch',
    },
   });